import { Injectable } from '@nestjs/common';
import { Company } from './company/company';

@Injectable()
export class AppService {
  getHello(): Company {
    return { name: 'Livenexx' };
  }
}
