import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { ApiResponse } from '../dto/api-response.dto';

@Catch(HttpException)
export class ApiExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    //const request = ctx.getRequest();
    const statusCode = exception.getStatus();

    const apiResponse = new ApiResponse(null, {
      success: false,
      message: 'Erreur de manipulation de ressource', //exception.name ||
      messageCode: exception.stack,
      clientTime: new Date().toLocaleString('fr-FR', {
        timeZone: 'Africa/Nairobi',
      }),
      code: statusCode.toString(),
    });

    response.status(200).json(apiResponse);
  }
}
