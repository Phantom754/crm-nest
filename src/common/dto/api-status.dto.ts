export class Status {
  code: string;
  success: boolean;
  message: string | null;
  clientTime: string | Date;
  messageCode: string | null;

  constructor(
    code: string,
    success: boolean,
    message: string | null,
    clienTime: Date | string,
    messageCode: string | null,
  ) {
    this.clientTime = clienTime;
    this.code = code;
    this.message = message;
    this.success = success;
    this.messageCode = messageCode;
  }
}
