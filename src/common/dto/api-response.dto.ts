import { Status } from './api-status.dto';

export class ApiResponse<T> {
  data: T;
  status: Status;

  constructor(data: T, status: Status) {
    this.data = data;
    this.status = status;
  }
}
