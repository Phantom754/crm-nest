// src/common/interceptors/api-response.interceptor.ts

import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiResponse } from '../dto/api-response.dto';

@Injectable()
export class ApiResponseInterceptor<T>
  implements NestInterceptor<T, ApiResponse<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<ApiResponse<T>> {
    return next.handle().pipe(
      map((data) => {
        return {
          data: data?.res || data,
          status: {
            message: data?.message || '',
            messageCode: '',
            success: true,
            clientTime: new Date().toLocaleString('fr-FR', {
              timeZone: 'Africa/Nairobi',
            }),
            code:
              `${context.switchToHttp().getResponse()?.statusCode}` || 'Succes',
          },
          payload: {
            
          }
        };
      }),
    );
  }
}
