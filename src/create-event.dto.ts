import { PartialType } from "@nestjs/mapped-types";

export class CreateEvent {
  name: string;
  description: string;
  when: string;
  adress: string;
}

export class UpdateEvent extends PartialType(CreateEvent) {}
