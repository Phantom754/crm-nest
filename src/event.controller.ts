import { Body, Controller, Get, Param, Patch, Post, Query } from "@nestjs/common";
import { CreateEvent, UpdateEvent } from "./create-event.dto";
import { Event } from "./event.entity";

@Controller({
  path: 'events',
})
export class EventController {
  private  events: Event[] = [
    {
      id: 1,
      name: "ddd dddd",
      description: 'LIVENEXX',
      adress: 'Antsakaviro',
      when: new Date(),
    }
  ];
  @Get()
  findAll() {
    return { res: this.events, message: 'liste company' };
  }

  @Get(':id')
  findOne(@Param('id') id: string, @Query() data: any) {
    return this.events.find(data => data.id == Number(id));
  }

  @Post()
  create(@Body() data: CreateEvent) {
    return { res: data, message: "ajout d'evenement avec success" };
  }

  @Patch(':id')
  update( @Param('id') id: string, @Body() data: UpdateEvent) {
   return { res: {...data, id} };
  }
}
