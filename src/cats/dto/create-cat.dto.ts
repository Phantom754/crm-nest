import { ApiProperty } from '@nestjs/swagger';

export class CreateCatDto {
  @ApiProperty({
    required: false,
    readOnly: true,
  })
  name: string;
}
